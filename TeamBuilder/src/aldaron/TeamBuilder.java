package aldaron;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

import javax.swing.JPanel;

import javax.swing.JTable;


public class TeamBuilder {


	public static void main(String[] args) {
		initializeUI();  
	}

	private static void initializeUI() {
		
		/******************************************************************************************
		 * 
		 * Define UI components
		 * 
		 * *****************************************************************************************/
				//overall frame for UI
				JFrame myJFrame = new JFrame ("Team Builder");
				
				//drop down lists for dual types and abilities for all 6 pokemon
				final JComboBox poke1Type1;
				final JComboBox poke1Type2;
				final JComboBox poke1Abilities;
				final JComboBox poke2Type1;
				final JComboBox poke2Type2;
				final JComboBox poke2Abilities;
				final JComboBox poke3Type1;
				final JComboBox poke3Type2;
				final JComboBox poke3Abilities;
				final JComboBox poke4Type1;
				final JComboBox poke4Type2;
				final JComboBox poke4Abilities;
				final JComboBox poke5Type1;
				final JComboBox poke5Type2;
				final JComboBox poke5Abilities;
				final JComboBox poke6Type1;
				final JComboBox poke6Type2;
				final JComboBox poke6Abilities;
				
				//Panel, with vertical layout, to hold 6 panels, with horizontal layouts, that hold the number, types, and ability
				JPanel myJPanel = new JPanel();
			    myJPanel.setLayout(new BoxLayout(myJPanel, BoxLayout.Y_AXIS));
			    
			    JPanel firstPokePanel = new JPanel();
			    JPanel secondPokePanel = new JPanel();
			    JPanel thirdPokePanel = new JPanel();
			    JPanel fourthPokePanel = new JPanel();
			    JPanel fifthPokePanel = new JPanel();
			    JPanel sixthPokePanel = new JPanel();
			    JLabel p1, p2, p3, p4, p5, p6;
			    
			    //submission button
			    JButton submit = new JButton("Submit");
			    
			    //two arrays define the types and relevant abilities
			      String types[] = {"Select Type", "None", "Bug", "Dark", "Dragon", "Electric", "Fairy", 
			    		  			"Fighting", "Fire", "Flying", "Ghost", "Grass", 
			    		  			"Ground", "Ice", "Normal", "Poison", "Psychic", "Rock",
			    		  			 "Steel", "Water"};
			      
			      String relevantAbilities[] = {"Select Ability", "None", "Dry Skin", "Flash Fire", "Heatproof", "Levitate", "LightningRod", 
			    		  						"Motor Drive", "Sap Sipper", "Storm Drain", "Thick Fat", "Volt Absorb", 
			    		  						"Water Absorb", "Wonder Guard"};
			      
			      String columns[] = {"-----", "Immune", "4X Resist", "2X resist", "Neutral", "2X Weak","4X Weak", "Total Resists", "Total Weaks", "------"};
			      
			      Object[][] data = {{"Bug", "0", "0", "0", "0", "0", "0", "0", "0","Bug"}, {"Dark", "0", "0", "0", "0", "0", "0", "0", "0","Dark"}, 
			    		             {"Dragon", "0", "0", "0", "0", "0", "0", "0", "0","Dragon"}, {"Electric", "0", "0", "0", "0", "0", "0", "0", "0","Electric"}, 
			    		             {"Fairy", "0", "0", "0", "0", "0", "0", "0", "0","Fairy"}, {"Fighting", "0", "0", "0", "0", "0", "0", "0", "0","Fighting"}, 
			    		             {"Fire", "0", "0", "0", "0", "0", "0", "0", "0","Fire"}, {"Flying", "0", "0", "0", "0", "0", "0", "0", "0","Flying"}, 
			    		             {"Ghost", "0", "0", "0", "0", "0", "0", "0", "0","Ghost"}, {"Grass", "0", "0", "0", "0", "0", "0", "0", "0","Grass"}, 
			    		             {"Ground", "0", "0", "0", "0", "0", "0", "0", "0","Ground"}, {"Ice", "0", "0", "0", "0", "0", "0", "0", "0","Ice"}, 
			    		             {"Normal", "0", "0", "0", "0", "0", "0", "0", "0","Normal"}, {"Poison", "0", "0", "0", "0", "0", "0", "0", "0","Poison"}, 
			    		             {"Psychic", "0", "0", "0", "0", "0", "0", "0", "0","Psychic"}, {"Rock", "0", "0", "0", "0", "0", "0", "0", "0","Rock"},
			    		             {"Steel", "0", "0", "0", "0", "0", "0", "0", "0","Steel"}, {"Water", "0", "0", "0", "0", "0", "0", "0", "0","Water"}};
			      
			      final JTable displayTable = new JTable (data, columns);
			      displayTable.getTableHeader().setReorderingAllowed(false);
			     
			      //define the labels and combo boxes for each pokemon
			      p1 = new JLabel("Pokemon 1: ");
			      poke1Type1 = new JComboBox(types);
			      poke1Type1.setSelectedIndex(0);
			      poke1Type2 = new JComboBox(types);
			      poke1Type2.setSelectedIndex(0);
			      poke1Abilities = new JComboBox(relevantAbilities);
			      poke1Abilities.setSelectedIndex(0);
			      
			      p2 = new JLabel("Pokemon 2: ");
			      poke2Type1 = new JComboBox(types);
			      poke2Type1.setSelectedIndex(0);
			      poke2Type2 = new JComboBox(types);
			      poke2Type2.setSelectedIndex(0);
			      poke2Abilities = new JComboBox(relevantAbilities);
			      poke2Abilities.setSelectedIndex(0);
			      
			      p3 = new JLabel("Pokemon 3: ");
			      poke3Type1 = new JComboBox(types);
			      poke3Type1.setSelectedIndex(0);
			      poke3Type2 = new JComboBox(types);
			      poke3Type2.setSelectedIndex(0);
			      poke3Abilities = new JComboBox(relevantAbilities);
			      poke3Abilities.setSelectedIndex(0);
			      
			      p4 = new JLabel("Pokemon 4: ");
			      poke4Type1 = new JComboBox(types);
			      poke4Type1.setSelectedIndex(0);
			      poke4Type2 = new JComboBox(types);
			      poke4Type2.setSelectedIndex(0);
			      poke4Abilities = new JComboBox(relevantAbilities);
			      poke4Abilities.setSelectedIndex(0);
			      
			      p5 = new JLabel("Pokemon 5: ");
			      poke5Type1 = new JComboBox(types);
			      poke5Type1.setSelectedIndex(0);
			      poke5Type2 = new JComboBox(types);
			      poke5Type2.setSelectedIndex(0);
			      poke5Abilities = new JComboBox(relevantAbilities);
			      poke5Abilities.setSelectedIndex(0);
			      
			      p6 = new JLabel("Pokemon 6: ");
			      poke6Type1 = new JComboBox(types);
			      poke6Type1.setSelectedIndex(0);
			      poke6Type2 = new JComboBox(types);
			      poke6Type2.setSelectedIndex(0);
			      poke6Abilities = new JComboBox(relevantAbilities);
			      poke6Abilities.setSelectedIndex(0);
			     
			      //add the comboboxes and labels to the appropriate panels
			      firstPokePanel.add(p1);
			      firstPokePanel.add(poke1Type1);
			      firstPokePanel.add(poke1Type2);
			      firstPokePanel.add(poke1Abilities);
			   
			      secondPokePanel.add(p2);
			      secondPokePanel.add(poke2Type1);
			      secondPokePanel.add(poke2Type2);
			      secondPokePanel.add(poke2Abilities);
			   
			      thirdPokePanel.add(p3);
			      thirdPokePanel.add(poke3Type1);
			      thirdPokePanel.add(poke3Type2);
			      thirdPokePanel.add(poke3Abilities);
			    
			      fourthPokePanel.add(p4);
			      fourthPokePanel.add(poke4Type1);
			      fourthPokePanel.add(poke4Type2);
			      fourthPokePanel.add(poke4Abilities);
			    
			      fifthPokePanel.add(p5);
			      fifthPokePanel.add(poke5Type1);
			      fifthPokePanel.add(poke5Type2);
			      fifthPokePanel.add(poke5Abilities);
			    
			      sixthPokePanel.add(p6);
			      sixthPokePanel.add(poke6Type1);
			      sixthPokePanel.add(poke6Type2);
			      sixthPokePanel.add(poke6Abilities);   
			      
			     
			      
			      //add all the Panels and separate components to the overall Panel
			     
			      myJPanel.add(firstPokePanel);
			      myJPanel.add(secondPokePanel);
			      myJPanel.add(thirdPokePanel);
			      myJPanel.add(fourthPokePanel);
			      myJPanel.add(fifthPokePanel);
			      myJPanel.add(sixthPokePanel);
			      myJPanel.add(submit);
			      myJPanel.add(displayTable.getTableHeader());
			      myJPanel.add(displayTable);
			      myJFrame.setContentPane(myJPanel);
			      
			  /******************************************************************************************
			   * 
			   * 
			   * ****************************************************************************************/
			      
			      
			      
			  /**********************************************************************************************
			   * 
			   * Submit button code, which calculates everything
			   * 
			   * */
			      
			      submit.addActionListener(new ActionListener() {
			    	  public void actionPerformed (ActionEvent e) {
			    		  //the arrays that will hold the resistance number arrays returned from the method
			    		  double[] firstPokeArray = new double[18];
			    		  double[] secondPokeArray = new double[18];
			    		  double[] thirdPokeArray = new double[18];
			    		  double[] fourthPokeArray = new double[18];
			    		  double[] fifthPokeArray = new double[18];
			    		  double[] sixthPokeArray = new double[18];
			    		  
			    		  //the counters for the display chart
			    		  int totalResists = 0, totalWeaks = 0;
			    		  
			    		  //clears the display chart
			    		  for(int row = 0; row < 18; row++){
			    			  for(int col = 1; col < 9; col++){
			    				  displayTable.setValueAt(0, row, col);
			    			  }
			    		  }
			    		  //putting the values into the appropriate arrays, with input from user's combo box choices
			    		 firstPokeArray = getTypeResists(poke1Type1.getSelectedItem().toString(), poke1Type2.getSelectedItem().toString(), poke1Abilities.getSelectedItem().toString());
			    		 secondPokeArray = getTypeResists(poke2Type1.getSelectedItem().toString(), poke2Type2.getSelectedItem().toString(), poke2Abilities.getSelectedItem().toString());
			    		 thirdPokeArray = getTypeResists(poke3Type1.getSelectedItem().toString(), poke3Type2.getSelectedItem().toString(), poke3Abilities.getSelectedItem().toString());
			    		 fourthPokeArray = getTypeResists(poke4Type1.getSelectedItem().toString(), poke4Type2.getSelectedItem().toString(), poke4Abilities.getSelectedItem().toString());
			    		 fifthPokeArray = getTypeResists(poke5Type1.getSelectedItem().toString(), poke5Type2.getSelectedItem().toString(), poke5Abilities.getSelectedItem().toString());
			    		 sixthPokeArray = getTypeResists(poke6Type1.getSelectedItem().toString(), poke6Type2.getSelectedItem().toString(), poke6Abilities.getSelectedItem().toString());
			    		  
			    		 //loops through 18 times, once for each type, counters how many resists / weaks / immunities / neutralities there are, and updates the chart
			    		  for(int i = 0; i < 18; i++){
			    			  //for immunity
			    			  if(firstPokeArray[i] == 0 || secondPokeArray[i] == 0 || thirdPokeArray[i] == 0 
			    					  || fourthPokeArray[i] == 0 || fifthPokeArray[i] == 0 || sixthPokeArray[i] == 0){
			    				  int tempCounter = 0;
			    				  if(firstPokeArray[i] == 0) tempCounter++;
			    				  if(secondPokeArray[i] == 0) tempCounter++;
			    				  if(thirdPokeArray[i] == 0) tempCounter++;
			    				  if(fourthPokeArray[i] == 0) tempCounter++;
			    				  if(fifthPokeArray[i] == 0) tempCounter++;
			    				  if(sixthPokeArray[i] == 0) tempCounter++;
			    				  switch (i) {
			    				  case 0:	displayTable.setValueAt(tempCounter, 0, 1);
			    				  			break;
			    				  case 1:	displayTable.setValueAt(tempCounter, 1, 1);
			    				  			break;
			    				  case 2:	displayTable.setValueAt(tempCounter, 2, 1);
	    				  					break;
			    				  case 3:	displayTable.setValueAt(tempCounter, 3, 1);
	    				  					break;
			    				  case 4:	displayTable.setValueAt(tempCounter, 4, 1);
	    				  					break;
			    				  case 5:	displayTable.setValueAt(tempCounter, 5, 1);
	    				  					break;
			    				  case 6:	displayTable.setValueAt(tempCounter, 6, 1);
	    				  					break;
			    				  case 7:	displayTable.setValueAt(tempCounter, 7, 1);
	    				  					break;
			    				  case 8:	displayTable.setValueAt(tempCounter, 8, 1);
	    				  					break;
			    				  case 9:	displayTable.setValueAt(tempCounter, 9, 1);
	    				  					break;
			    				  case 10:	displayTable.setValueAt(tempCounter, 10, 1);
	    				  					break;
			    				  case 11:	displayTable.setValueAt(tempCounter, 11, 1);
	    				  					break;
			    				  case 12:	displayTable.setValueAt(tempCounter, 12, 1);
	    				  					break;
			    				  case 13:	displayTable.setValueAt(tempCounter, 13, 1);
	    				  					break;
			    				  case 14:	displayTable.setValueAt(tempCounter, 14, 1);
	    				  					break;
			    				  case 15:	displayTable.setValueAt(tempCounter, 15, 1);
	    				  					break;
			    				  case 16:	displayTable.setValueAt(tempCounter, 16, 1);
	    				  					break;
			    				  case 17:	displayTable.setValueAt(tempCounter, 17, 1);
	    				  					break;
			    					  
			    				  }
			    			  }
			    			  //for 1/8 resist, but treats like 1/4 to save screen real estate on UI
			    			  if(firstPokeArray[i] == .125 || secondPokeArray[i] == .125|| thirdPokeArray[i] == .125 
			    					  || fourthPokeArray[i] == .125 || fifthPokeArray[i] == .125 || sixthPokeArray[i] == .125) {
			    				  int tempCounter = 0;
			    				  if(firstPokeArray[i] == .125) tempCounter++;
			    				  if(secondPokeArray[i] == .125) tempCounter++;
			    				  if(thirdPokeArray[i] == .125) tempCounter++;
			    				  if(fourthPokeArray[i] == .125) tempCounter++;
			    				  if(fifthPokeArray[i] == .125) tempCounter++;
			    				  if(sixthPokeArray[i] == .125) tempCounter++;
			    				  switch (i) {
			    				  case 0:	displayTable.setValueAt(tempCounter, 0, 2);
			    				  			break;
			    				  case 1:	displayTable.setValueAt(tempCounter, 1, 2);
			    				  			break;
			    				  case 2:	displayTable.setValueAt(tempCounter, 2, 2);
	    				  					break;
			    				  case 3:	displayTable.setValueAt(tempCounter, 3, 2);
	    				  					break;
			    				  case 4:	displayTable.setValueAt(tempCounter, 4, 2);
	    				  					break;
			    				  case 5:	displayTable.setValueAt(tempCounter, 5, 2);
	    				  					break;
			    				  case 6:	displayTable.setValueAt(tempCounter, 6, 2);
	    				  					break;
			    				  case 7:	displayTable.setValueAt(tempCounter, 7, 2);
	    				  					break;
			    				  case 8:	displayTable.setValueAt(tempCounter, 8, 2);
	    				  					break;
			    				  case 9:	displayTable.setValueAt(tempCounter, 9, 2);
	    				  					break;
			    				  case 10:	displayTable.setValueAt(tempCounter, 10, 2);
	    				  					break;
			    				  case 11:	displayTable.setValueAt(tempCounter, 11,2);
	    				  					break;
			    				  case 12:	displayTable.setValueAt(tempCounter, 12, 2);
	    				  					break;
			    				  case 13:	displayTable.setValueAt(tempCounter, 13, 2);
	    				  					break;
			    				  case 14:	displayTable.setValueAt(tempCounter, 14, 2);
	    				  					break;
			    				  case 15:	displayTable.setValueAt(tempCounter, 15, 2);
	    				  					break;
			    				  case 16:	displayTable.setValueAt(tempCounter, 16, 2);
	    				  					break;
			    				  case 17:	displayTable.setValueAt(tempCounter, 17, 2);
	    				  					break;
			    					  
			    				  }
			    			  }
			    			  //for 1/4 resist 
			    			  if(firstPokeArray[i] == .25 || secondPokeArray[i] == .25 || thirdPokeArray[i] == .25 
			    					  || fourthPokeArray[i] == .25 || fifthPokeArray[i] == .25 || sixthPokeArray[i] == .25){
			    				  int tempCounter = 0;
			    				  if(firstPokeArray[i] == .25) tempCounter++;
			    				  if(secondPokeArray[i] == .25) tempCounter++;
			    				  if(thirdPokeArray[i] == .25) tempCounter++;
			    				  if(fourthPokeArray[i] == .25) tempCounter++;
			    				  if(fifthPokeArray[i] == .25) tempCounter++;
			    				  if(sixthPokeArray[i] == .25) tempCounter++;
			    				  switch (i) {
			    				  case 0:	displayTable.setValueAt(tempCounter, 0, 2);
			    				  			break;
			    				  case 1:	displayTable.setValueAt(tempCounter, 1, 2);
			    				  			break;
			    				  case 2:	displayTable.setValueAt(tempCounter, 2, 2);
	    				  					break;
			    				  case 3:	displayTable.setValueAt(tempCounter, 3, 2);
	    				  					break;
			    				  case 4:	displayTable.setValueAt(tempCounter, 4, 2);
	    				  					break;
			    				  case 5:	displayTable.setValueAt(tempCounter, 5, 2);
	    				  					break;
			    				  case 6:	displayTable.setValueAt(tempCounter, 6, 2);
	    				  					break;
			    				  case 7:	displayTable.setValueAt(tempCounter, 7, 2);
	    				  					break;
			    				  case 8:	displayTable.setValueAt(tempCounter, 8, 2);
	    				  					break;
			    				  case 9:	displayTable.setValueAt(tempCounter, 9, 2);
	    				  					break;
			    				  case 10:	displayTable.setValueAt(tempCounter, 10, 2);
	    				  					break;
			    				  case 11:	displayTable.setValueAt(tempCounter, 11, 2);
	    				  					break;
			    				  case 12:	displayTable.setValueAt(tempCounter, 12, 2);
	    				  					break;
			    				  case 13:	displayTable.setValueAt(tempCounter, 13, 2);
	    				  					break;
			    				  case 14:	displayTable.setValueAt(tempCounter, 14, 2);
	    				  					break;
			    				  case 15:	displayTable.setValueAt(tempCounter, 15, 2);
	    				  					break;
			    				  case 16:	displayTable.setValueAt(tempCounter, 16, 2);
	    				  					break;
			    				  case 17:	displayTable.setValueAt(tempCounter, 17, 2);
	    				  					break;
			    					  
			    				  }
			    			  }
			    			  //for normal resist
			    			  if(firstPokeArray[i] == .5 || secondPokeArray[i] == .5|| thirdPokeArray[i] == .5 
			    					  || fourthPokeArray[i] == .5 || fifthPokeArray[i] == .5 || sixthPokeArray[i] == .5) {
			    				  int tempCounter = 0;
			    				  if(firstPokeArray[i] == .5) tempCounter++;
			    				  if(secondPokeArray[i] == .5) tempCounter++;
			    				  if(thirdPokeArray[i] == .5) tempCounter++;
			    				  if(fourthPokeArray[i] == .5) tempCounter++;
			    				  if(fifthPokeArray[i] == .5) tempCounter++;
			    				  if(sixthPokeArray[i] == .5) tempCounter++;
			    				  switch (i) {
			    				  case 0:	displayTable.setValueAt(tempCounter, 0, 3);
			    				  			break;
			    				  case 1:	displayTable.setValueAt(tempCounter, 1, 3);
			    				  			break;
			    				  case 2:	displayTable.setValueAt(tempCounter, 2, 3);
	    				  					break;
			    				  case 3:	displayTable.setValueAt(tempCounter, 3, 3);
	    				  					break;
			    				  case 4:	displayTable.setValueAt(tempCounter, 4, 3);
	    				  					break;
			    				  case 5:	displayTable.setValueAt(tempCounter, 5, 3);
	    				  					break;
			    				  case 6:	displayTable.setValueAt(tempCounter, 6, 3);
	    				  					break;
			    				  case 7:	displayTable.setValueAt(tempCounter, 7, 3);
	    				  					break;
			    				  case 8:	displayTable.setValueAt(tempCounter, 8, 3);
	    				  					break;
			    				  case 9:	displayTable.setValueAt(tempCounter, 9, 3);
	    				  					break;
			    				  case 10:	displayTable.setValueAt(tempCounter, 10, 3);
	    				  					break;
			    				  case 11:	displayTable.setValueAt(tempCounter, 11, 3);
	    				  					break;
			    				  case 12:	displayTable.setValueAt(tempCounter, 12, 3);
	    				  					break;
			    				  case 13:	displayTable.setValueAt(tempCounter, 13, 3);
	    				  					break;
			    				  case 14:	displayTable.setValueAt(tempCounter, 14, 3);
	    				  					break;
			    				  case 15:	displayTable.setValueAt(tempCounter, 15, 3);
	    				  					break;
			    				  case 16:	displayTable.setValueAt(tempCounter, 16, 3);
	    				  					break;
			    				  case 17:	displayTable.setValueAt(tempCounter, 17, 3);
	    				  					break;
			    					  
			    				  }
			    			  }
			    			  //for neutrality
			    			  if(firstPokeArray[i] == 1 || secondPokeArray[i] == 1|| thirdPokeArray[i] == 1 
			    					  || fourthPokeArray[i] == 1 || fifthPokeArray[i] == 1 || sixthPokeArray[i] == 1) {
			    				  int tempCounter = 0;
			    				  if(firstPokeArray[i] == 1) tempCounter++;
			    				  if(secondPokeArray[i] == 1) tempCounter++;
			    				  if(thirdPokeArray[i] == 1) tempCounter++;
			    				  if(fourthPokeArray[i] == 1) tempCounter++;
			    				  if(fifthPokeArray[i] == 1) tempCounter++;
			    				  if(sixthPokeArray[i] == 1) tempCounter++;
			    				  switch (i) {
			    				  case 0:	displayTable.setValueAt(tempCounter, 0, 4);
			    				  			break;
			    				  case 1:	displayTable.setValueAt(tempCounter, 1, 4);
			    				  			break;
			    				  case 2:	displayTable.setValueAt(tempCounter, 2, 4);
	    				  					break;
			    				  case 3:	displayTable.setValueAt(tempCounter, 3, 4);
	    				  					break;
			    				  case 4:	displayTable.setValueAt(tempCounter, 4, 4);
	    				  					break;
			    				  case 5:	displayTable.setValueAt(tempCounter, 5, 4);
	    				  					break;
			    				  case 6:	displayTable.setValueAt(tempCounter, 6, 4);
	    				  					break;
			    				  case 7:	displayTable.setValueAt(tempCounter, 7, 4);
	    				  					break;
			    				  case 8:	displayTable.setValueAt(tempCounter, 8, 4);
	    				  					break;
			    				  case 9:	displayTable.setValueAt(tempCounter, 9, 4);
	    				  					break;
			    				  case 10:	displayTable.setValueAt(tempCounter, 10, 4);
	    				  					break;
			    				  case 11:	displayTable.setValueAt(tempCounter, 11, 4);
	    				  					break;
			    				  case 12:	displayTable.setValueAt(tempCounter, 12, 4);
	    				  					break;
			    				  case 13:	displayTable.setValueAt(tempCounter, 13, 4);
	    				  					break;
			    				  case 14:	displayTable.setValueAt(tempCounter, 14, 4);
	    				  					break;
			    				  case 15:	displayTable.setValueAt(tempCounter, 15, 4);
	    				  					break;
			    				  case 16:	displayTable.setValueAt(tempCounter, 16, 4);
	    				  					break;
			    				  case 17:	displayTable.setValueAt(tempCounter, 17, 4);
	    				  					break;
			    					  
			    				  }
			    			  }
			    			  //for standard weakness
			    			  if(firstPokeArray[i] == 2 || secondPokeArray[i] == 2|| thirdPokeArray[i] == 2 
			    					  || fourthPokeArray[i] == 2 || fifthPokeArray[i] == 2 || sixthPokeArray[i] == 2) {
			    				  int tempCounter = 0;
			    				  if(firstPokeArray[i] == 2) tempCounter++;
			    				  if(secondPokeArray[i] == 2) tempCounter++;
			    				  if(thirdPokeArray[i] == 2) tempCounter++;
			    				  if(fourthPokeArray[i] == 2) tempCounter++;
			    				  if(fifthPokeArray[i] == 2) tempCounter++;
			    				  if(sixthPokeArray[i] == 2) tempCounter++;
			    				  switch (i) {
			    				  case 0:	displayTable.setValueAt(tempCounter, 0, 5);
			    				  			break;
			    				  case 1:	displayTable.setValueAt(tempCounter, 1, 5);
			    				  			break;
			    				  case 2:	displayTable.setValueAt(tempCounter, 2, 5);
	    				  					break;
			    				  case 3:	displayTable.setValueAt(tempCounter, 3, 5);
	    				  					break;
			    				  case 4:	displayTable.setValueAt(tempCounter, 4, 5);
	    				  					break;
			    				  case 5:	displayTable.setValueAt(tempCounter, 5, 5);
	    				  					break;
			    				  case 6:	displayTable.setValueAt(tempCounter, 6, 5);
	    				  					break;
			    				  case 7:	displayTable.setValueAt(tempCounter, 7, 5);
	    				  					break;
			    				  case 8:	displayTable.setValueAt(tempCounter, 8, 5);
	    				  					break;
			    				  case 9:	displayTable.setValueAt(tempCounter, 9, 5);
	    				  					break;
			    				  case 10:	displayTable.setValueAt(tempCounter, 10, 5);
	    				  					break;
			    				  case 11:	displayTable.setValueAt(tempCounter, 11, 5);
	    				  					break;
			    				  case 12:	displayTable.setValueAt(tempCounter, 12, 5);
	    				  					break;
			    				  case 13:	displayTable.setValueAt(tempCounter, 13, 5);
	    				  					break;
			    				  case 14:	displayTable.setValueAt(tempCounter, 14, 5);
	    				  					break;
			    				  case 15:	displayTable.setValueAt(tempCounter, 15, 5);
	    				  					break;
			    				  case 16:	displayTable.setValueAt(tempCounter, 16, 5);
	    				  					break;
			    				  case 17:	displayTable.setValueAt(tempCounter, 17, 5);
	    				  					break;
			    					  
			    				  }
			    			  }
			    			  //for *4 weakness
			    			  if(firstPokeArray[i] == 4 || secondPokeArray[i] == 4|| thirdPokeArray[i] == 4 
			    					  || fourthPokeArray[i] == 4 || fifthPokeArray[i] == 4 || sixthPokeArray[i] == 4) {
			    				  int tempCounter = 0;
			    				  if(firstPokeArray[i] == 4) tempCounter++;
			    				  if(secondPokeArray[i] == 4) tempCounter++;
			    				  if(thirdPokeArray[i] == 4) tempCounter++;
			    				  if(fourthPokeArray[i] == 4) tempCounter++;
			    				  if(fifthPokeArray[i] == 4) tempCounter++;
			    				  if(sixthPokeArray[i] == 4) tempCounter++;
			    				  switch (i) {
			    				  case 0:	displayTable.setValueAt(tempCounter, 0, 6);
			    				  			break;
			    				  case 1:	displayTable.setValueAt(tempCounter, 1, 6);
			    				  			break;
			    				  case 2:	displayTable.setValueAt(tempCounter, 2, 6);
	    				  					break;
			    				  case 3:	displayTable.setValueAt(tempCounter, 3, 6);
	    				  					break;
			    				  case 4:	displayTable.setValueAt(tempCounter, 4, 6);
	    				  					break;
			    				  case 5:	displayTable.setValueAt(tempCounter, 5, 6);
	    				  					break;
			    				  case 6:	displayTable.setValueAt(tempCounter, 6, 6);
	    				  					break;
			    				  case 7:	displayTable.setValueAt(tempCounter, 7, 6);
	    				  					break;
			    				  case 8:	displayTable.setValueAt(tempCounter, 8, 6);
	    				  					break;
			    				  case 9:	displayTable.setValueAt(tempCounter, 9, 6);
	    				  					break;
			    				  case 10:	displayTable.setValueAt(tempCounter, 10, 6);
	    				  					break;
			    				  case 11:	displayTable.setValueAt(tempCounter, 11,6);
	    				  					break;
			    				  case 12:	displayTable.setValueAt(tempCounter, 12, 6);
	    				  					break;
			    				  case 13:	displayTable.setValueAt(tempCounter, 13, 6);
	    				  					break;
			    				  case 14:	displayTable.setValueAt(tempCounter, 14, 6);
	    				  					break;
			    				  case 15:	displayTable.setValueAt(tempCounter, 15, 6);
	    				  					break;
			    				  case 16:	displayTable.setValueAt(tempCounter, 16, 6);
	    				  					break;
			    				  case 17:	displayTable.setValueAt(tempCounter, 17, 6);
	    				  					break;
			    					  
			    				  }
			    			  }
			    			  //for *8 weakness, but treats like 4* weakness to save UI real estate 
			    			  if(firstPokeArray[i] == 8 || secondPokeArray[i] == 8|| thirdPokeArray[i] == 8 
			    					  || fourthPokeArray[i] == 8 || fifthPokeArray[i] == 8 || sixthPokeArray[i] == 8) {
			    				  int tempCounter = 0;
			    				  if(firstPokeArray[i] == 8) tempCounter++;
			    				  if(secondPokeArray[i] == 8) tempCounter++;
			    				  if(thirdPokeArray[i] == 8) tempCounter++;
			    				  if(fourthPokeArray[i] == 8) tempCounter++;
			    				  if(fifthPokeArray[i] == 8) tempCounter++;
			    				  if(sixthPokeArray[i] == 8) tempCounter++;
			    				  switch (i) {
			    				  case 0:	displayTable.setValueAt(tempCounter, 0, 6);
			    				  			break;
			    				  case 1:	displayTable.setValueAt(tempCounter, 1, 6);
			    				  			break;
			    				  case 2:	displayTable.setValueAt(tempCounter, 2, 6);
	    				  					break;
			    				  case 3:	displayTable.setValueAt(tempCounter, 3, 6);
	    				  					break;
			    				  case 4:	displayTable.setValueAt(tempCounter, 4, 6);
	    				  					break;
			    				  case 5:	displayTable.setValueAt(tempCounter, 5, 6);
	    				  					break;
			    				  case 6:	displayTable.setValueAt(tempCounter, 6, 6);
	    				  					break;
			    				  case 7:	displayTable.setValueAt(tempCounter, 7, 6);
	    				  					break;
			    				  case 8:	displayTable.setValueAt(tempCounter, 8, 6);
	    				  					break;
			    				  case 9:	displayTable.setValueAt(tempCounter, 9, 6);
	    				  					break;
			    				  case 10:	displayTable.setValueAt(tempCounter, 10, 6);
	    				  					break;
			    				  case 11:	displayTable.setValueAt(tempCounter, 11,6);
	    				  					break;
			    				  case 12:	displayTable.setValueAt(tempCounter, 12, 6);
	    				  					break;
			    				  case 13:	displayTable.setValueAt(tempCounter, 13, 6);
	    				  					break;
			    				  case 14:	displayTable.setValueAt(tempCounter, 14, 6);
	    				  					break;
			    				  case 15:	displayTable.setValueAt(tempCounter, 15, 6);
	    				  					break;
			    				  case 16:	displayTable.setValueAt(tempCounter, 16, 6);
	    				  					break;
			    				  case 17:	displayTable.setValueAt(tempCounter, 17, 6);
	    				  					break;
			    					  
			    				  }
			    			  }
			    		  } 
			    		  
			    		  //aggregate the totals resists and weaknesses and display them on the table
			    		  for(int row = 0; row < 18; row++){
			    			  for(int col = 1; col < 9; col++){
			    				  if(col < 4){
			    					  totalResists+=Double.parseDouble(displayTable.getValueAt(row, col).toString());
			    				  }
			    				  if (col > 4){
			    					  totalWeaks+=Double.parseDouble(displayTable.getValueAt(row, col).toString());
			    				  }
			    			  }
			    			  displayTable.setValueAt(totalResists, row, 7);
			    			  displayTable.setValueAt(totalWeaks, row, 8);
			    			  totalResists = 0;
			    			  totalWeaks = 0;
			    		  } 
			    		  
			    	  }
			      });
			      
			      
			   /************************************************************************************************/
			      
			      
			      myJFrame.setSize(800,580);
			      myJFrame.setVisible(true);
			      myJFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	//responsible for returning the resistance / weakness number array
	protected static double[] getTypeResists(String type1, String type2, String ability) {
		//boolean checks that help me manipulate whether 1 legitimate, 2 legitimate, or neither legitimate types were passed in
		boolean check1 = false;
		boolean check2 = false;
		
		//type arrays responsible for holding the resistance values
		double[] firstArray = new double[18];
		double[] secondArray = new double[18];
		double[] finalTypeArray = new double[18];
		
		
		//values for double array charts: 0 is immune, .5 is resist, 1 is neutral, 2 is SE
		//not used but will use later; is array for left side attacking top
		String[][] offensiveTypeChart = {{"-", "Bug", "Dark", "Dragon", "Electric", "Fairy", "Fighting", "Fire", "Flying", "Ghost", 
							     "Grass", "Ground", "Ice", "Normal", "Poison", "Psychic", "Rock", "Steel", "Water"},
							     {"Bug", "1", "2", "1", "1", ".5", ".5", ".5", ".5", ".5", "2", "1", "1", "1", ".5", "2", "1", ".5", "1"}, 
							     {"Dark", "1", ".5", "1", "1", ".5", ".5", "1", "1", "2", "1", "1", "1", "1", "1", "2", "1", ".5", "1"}, 
		    		             {"Dragon", "1", "1", "2", "1", "0", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", ".5", "1"}, 
		    		             {"Electric", "1", "1", ".5", ".5", "1", "1", "1", "2", "1", ".5", "0", "1", "1", "1", "1", "1", "1", "2"}, 
		    		             {"Fairy", "1", "2", "2", "1", "1", "2", ".5", "1", "1", "1", "1", "1", "1", ".5", "1", "1", ".5", "1"}, 
		    		             {"Fighting", ".5", "2", "1", "1", ".5", "1", "1", ".5", "0", "1", "1", "2", "2", ".5", ".5", "2", "2", "1"}, 
		    		             {"Fire", "2", "1", ".5", "1", "1", "1", ".5", "1", "1", "2", "1", "2", "1", "1", "1", ".5", "2", ".5"}, 
		    		             {"Flying", "2", "1", "1", ".5", "1", "2", "1", "1", "1", "2", "1", "1", "1", "1", "1", ".5", ".5", "1"}, 
		    		             {"Ghost", "1", ".5", "1", "1", "1", "1", "1", "1", "2", "1", "1", "1", "0", "1", "2", "1", "1", "1"}, 
		    		             {"Grass", ".5", "1", ".5", "1", "1", "1", ".5", ".5", "1", ".5", "2", "1", "1", ".5", "1", "2", ".5", "2"}, 
		    		             {"Ground", ".5", "1", "1", "2", "1", "1", "2", "0", "1", ".5", "1", "1", "1", "2", "1", "2", "2", "1"}, 
		    		             {"Ice", "1", "1", "2", "1", "1", "1", ".5", "2", "1", "2", "2", ".5", "1", "1", "1", "1", ".5", ".5"}, 
		    		             {"Normal", "1", "1", "1", "1", "1", "1", "1", "1", "0", "1", "1", "1", "1", "1", "1", ".5", ".5", "1"}, 
		    		             {"Poison", "1", "1", "1", "1", "2", "1", "1", "1", ".5", "2", ".5", "1", "1", ".5", "1", ".5", "0", "1"}, 
		    		             {"Psychic", "1", "0", "1", "1", "1", "2", "1", "1", "1", "1", "1", "1", "1", "2", ".5", "1", ".5", "1"}, 
		    		             {"Rock", "2", "1", "1", "1", "1", ".5", "2", "2", "1", "1", ".5", "2", "1", "1", "1", "1", ".5", "1"},
		    		             {"Steel", "1", "1", "1", ".5", "2", "1", ".5", "1", "1", "1", "1", "2", "1", "1", "1", "2", ".5", ".5"}, 
		    		             {"Water", "1", "1", ".5", "1", "1", "1", "2", "1", "1", ".5", "2", "1", "1", "1", "1", "2", "1", ".5"}};
		
		//used for ease with java, since easier to read horizontally in an array than vertically, is top attacking left
		String[][] defensiveTypeChart = {{"-", "Bug", "Dark", "Dragon", "Electric", "Fairy", "Fighting", "Fire", "Flying", "Ghost", 
		     					"Grass", "Ground", "Ice", "Normal", "Poison", "Psychic", "Rock", "Steel", "Water"},
		     					{"Bug", "1", "1", "1", "1", "1", ".5", "2", "2", "1", ".5", ".5", "1", "1", "1", "1", "2", "1", "1"}, 
		     					{"Dark", "2", ".5", "1", "1", "2", "2", "1", "1", ".5", "1", "1", "1", "1", "1", "0", "1", "1", "1"}, 
		     					{"Dragon", "1", "1", "2", ".5", "2", "1", ".5", "1", "1", ".5", "1", "2", "1", "1", "1", "1", "1", ".5"}, 
		     					{"Electric", "1", "1", "1", ".5", "1", "1", "1", ".5", "1", "1", "2", "1", "1", "1", "1", "1", ".5", "1"}, 
		     					{"Fairy", ".5", ".5", "0", "1", "1", ".5", "1", "1", "1", "1", "1", "1", "1", "2", "1", "1", "2", "1"}, 
		     					{"Fighting", ".5", ".5", "1", "1", "2", "1", "1", "2", "1", "1", "1", "1", "1", "1", "2", ".5", "1", "1"}, 
		     					{"Fire", ".5", "1", "1", "1", ".5", "1", ".5", "1", "1", ".5", "2", ".5", "1", "1", "1", "2", ".5", "2"}, 
		     					{"Flying", ".5", "1", "1", "2", "1", ".5", "1", "1", "1", ".5", "0", "2", "1", "1", "1", "2", "1", "1"}, 
		     					{"Ghost", ".5", "2", "1", "1", "1", "0", "1", "1", "2", "1", "1", "1", "0", ".5", "1", "1", "1", "1"}, 
		     					{"Grass", "2", "1", "1", ".5", "1", "1", "2", "2", "1", ".5", ".5", "2", "1", "2", "1", "1", "1", ".5"}, 
		     					{"Ground", "1", "1", "1", "0", "1", "1", "1", "1", "1", "2", "1", "2", "1", ".5", "1", ".5", "1", "2"}, 
		     					{"Ice", "1", "1", "1", "1", "1", "2", "2", "1", "1", "1", "1", ".5", "1", "1", "1", "2", "2", "1"}, 
		     					{"Normal", "1", "1", "1", "1", "1", "2", "1", "1", "0", "1", "1", "1", "1", "1", "1", "1", "1", "1"}, 
		     					{"Poison", ".5", "1", "1", "1", ".5", ".5", "1", "1", "1", ".5", "2", "1", "1", ".5", "2", "1", "1", "1"}, 
		     					{"Psychic", "2", "2", "1", "1", "1", ".5", "1", "1", "2", "1", "1", "1", "1", "1", ".5", "1", "1", "1"}, 
		     					{"Rock", "1", "1", "1", "1", "1", "2", ".5", ".5", "1", "2", "2", "1", ".5", ".5", "1", "1", "2", "2"},
		     					{"Steel", ".5", ".5", ".5", "1", ".5", "2", "2", ".5", "1", ".5", "2", ".5", ".5", "0", ".5", ".5", ".5", "1"}, 
		     					{"Water", "1", "1", "1", "2", "1", "1", ".5", "1", "1", "2", "1", ".5", "1", "1", "1", "1", ".5", ".5"}};
		
		//loops through defensive chart and puts in a resistance array for each type
		for(int i = 0; i < defensiveTypeChart.length;i++){
			if(type1.equals(defensiveTypeChart[i][0])){
				check1 = true;
				//System.out.println("hi - type1");
				for(int j = 0; j < defensiveTypeChart[i].length-1;j++){
					firstArray[j] = Double.parseDouble(defensiveTypeChart[i][j+1]);
					
				}
			}
			if(type2.equals(defensiveTypeChart[i][0])){
				check2 = true;
				//System.out.println("hi - type2");
				for(int j = 0; j < defensiveTypeChart[i].length-1;j++){
					secondArray[j] = Double.parseDouble(defensiveTypeChart[i][j+1]);
				}
			}	
		}
		
		//conditionals combine the two arrays, or just use the single legitimate type passed in
		//multiplication logic for double types 
		if(check1 && check2){
			for(int x = 0; x < 18; x++){
				finalTypeArray[x] = firstArray[x] * secondArray[x];
				//System.out.println(finalTypeArray[x]);	
			}
			
		}
		if(check1 && !check2){
			for(int x = 0; x < 18; x++){
				finalTypeArray[x] = firstArray[x];
				//System.out.println(finalTypeArray[x]);	
			}
			
		}
		if(check2 && !check1){
			for(int x = 0; x < 18; x++){
				finalTypeArray[x] = secondArray[x];
				//System.out.println(finalTypeArray[x]);	
			}
		
		}
		
		if(!check2 && !check1){
			for(int x = 0; x < 18; x++){
				finalTypeArray[x] = 30.0;
				//System.out.println(finalTypeArray[x]);	
			}
		
		}
			
			//alter the resistance array depending on the ability
			switch (ability) {
				case "Dry Skin": finalTypeArray[6] = finalTypeArray[6] * 2.0;
								 finalTypeArray[17] = finalTypeArray[17] * 0.0;
								 break;
				case "Flash Fire":
								 finalTypeArray[6] = finalTypeArray[6] * 0.0;
								 break;
				case "Heatproof":
								 finalTypeArray[6] = finalTypeArray[6] / 2.0;
								 break;
				case "Levitate":
								 finalTypeArray[10] = finalTypeArray[10] * 0.0;
								 break;
				case "LightningRod":
								 finalTypeArray[3] = finalTypeArray[3] * 0.0;
								 break;
				case "Motor Drive":
								 finalTypeArray[3] = finalTypeArray[3] * 0.0;
								 break;
				case "Sap Sipper":
							     finalTypeArray[9] = finalTypeArray[9] * 0.0;
							     break;
				case "Storm Drain":
								 finalTypeArray[17] = finalTypeArray[17] * 0.0;
								 break;
				case "Thick Fat":
								 finalTypeArray[6] = finalTypeArray[6] / 2.0;
								 finalTypeArray[11] = finalTypeArray[11] / 2.0;
								 break;
				case "Volt Absorb":
								 finalTypeArray[3] = finalTypeArray[3] * 0.0;
								 break;
				case "Water Absorb":
								 finalTypeArray[17] = finalTypeArray[17] * 0.0;
								 break;
				case "Wonder Guard":
								for(int i = 0; i < 18; i++){
									if(!(finalTypeArray[i] == 2) && !(finalTypeArray[i] == 4) && !(finalTypeArray[i] == 8)){
										finalTypeArray[i] = 0.0;
									}
								}
								break;
			}
		return finalTypeArray;
	}

}